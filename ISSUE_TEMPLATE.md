### Description

[Description of the issue]

### Steps to Reproduce

1. [First Step]
2. [Second Step]
3. [and so on...]

**Expected behavior:** [What you expect to happen]

**Actual behavior:** [What actually happens]

**Reproduces how often:** [What percentage of the time does it reproduce?]

### Versions

Which version of Python are you running?
Which webdriver are you using?
Which version of the browser are you using?
Which OS + version are you using?

### Additional Information

Any additional information, configuration or data that might be necessary to reproduce the issue.