# YED
![Platform YED is currently made for](https://img.shields.io/badge/platform-windows-green.svg)

**YED** is a Python tool to automate the YACHT expense declarations.
Currently it is possible to perform these tasks automatically:
- Commute
- Public Transport


From the YED package folder, you can run this in command-line
```bash
python main.py
``` 

## Windows webdrivers
The below is a convenient way to obtain the webdrivers required for the browser(s) you're using.

- Install chocolatey guide: https://chocolatey.org/install
- A package repository can be found here: https://chocolatey.org/packages
- Install webdriver for your browser, e.g. 
  - "choco install selenium-chrome-driver"
  - "choco install selenium-gecko-driver"
- Modify driver_path inside config.py for your browsers webdriver location or add the location to your PATH - https://bit.ly/2COZBe6